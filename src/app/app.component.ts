import { AfterViewInit, Component, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { ThemeSwitchService } from './modules/shared/services/theme-switch.service';
import { SpinnerService } from './modules/shared/services/spinner.service';
import { LayoutService } from './modules/shared/services/layout.service';
import { NotificationService } from './modules/shared/services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent {
  title = 'market';

  showLoader$ = this._spinner.showLoader$.asObservable();
  loaderText$ = this._spinner.text$.asObservable();

  constructor(
    public themeService: ThemeSwitchService,
    public layoutService: LayoutService,
    public notifications: NotificationService,
    private _spinner: SpinnerService
  ) {
    moment.locale('ru');
  }
}
