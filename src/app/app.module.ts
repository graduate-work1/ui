import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  TuiRootModule,
  TuiDialogModule,
  TuiNotificationsModule,
  TuiSvgModule,
  TuiThemeNightModule,
  TuiLoaderModule,
  TuiModeModule,
  TUI_SANITIZER,
  TuiButtonModule,
} from '@taiga-ui/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './modules/shared/shared.module';
import { LandingComponent } from './components/landing/landing.component';
import { ErrorComponent } from './components/error/error.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { of } from 'rxjs';
import { InterceptorService } from './modules/shared/services/interceptor.service';
import { TUI_LANGUAGE, TUI_RUSSIAN_LANGUAGE } from '@taiga-ui/i18n';
import { NgDompurifySanitizer } from '@tinkoff/ng-dompurify';
import { AngularYandexMapsModule, YaConfig } from 'angular8-yandex-maps';
import { RecipeSharedModule } from './modules/recipe-shared/recipe-shared.module';

const mapConfig: YaConfig = {
  apikey: '87b90239-1e21-40e5-923f-66354c2aca09',
};

@NgModule({
  declarations: [AppComponent, LandingComponent, ErrorComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    TuiRootModule,
    BrowserAnimationsModule,
    TuiDialogModule,
    TuiNotificationsModule,
    SharedModule,
    TuiSvgModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    TuiThemeNightModule,
    TuiLoaderModule,
    TuiModeModule,
    TuiButtonModule,
    AngularYandexMapsModule.forRoot(mapConfig),
    RecipeSharedModule,
  ],
  providers: [
    { provide: TUI_SANITIZER, useClass: NgDompurifySanitizer },
    {
      provide: TUI_LANGUAGE,
      useValue: of(TUI_RUSSIAN_LANGUAGE),
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    },
  ],

  bootstrap: [AppComponent],
})
export class AppModule {}
