import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { RoleGuard } from '../shared/guards/role-guard.service';
import { UserRoleEnum } from '../shared/models/user-role.enum';
import { UsersGridComponent } from './components/users-grid/users-grid.component';
import { OrganizationsGridComponent } from './components/organizations-grid/organizations-grid.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [RoleGuard],
    data: {
      role: UserRoleEnum.Admin,
    },
    children: [
      { path: 'users', component: UsersGridComponent },
      { path: 'organizations', component: OrganizationsGridComponent },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'users',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
