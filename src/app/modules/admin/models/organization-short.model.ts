export class OrganizationShort {
  public id!: string;
  public name!: string;
  public employeesCount!: number;
  public salePointsCount!: number;
}
