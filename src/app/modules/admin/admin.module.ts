import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { SharedModule } from '../shared/shared.module';
import { UsersGridComponent } from './components/users-grid/users-grid.component';
import {
  TuiTableModule,
  TuiTablePaginationModule,
} from '@taiga-ui/addon-table';
import {
  TuiButtonModule,
  TuiLinkModule,
  TuiTextfieldControllerModule,
} from '@taiga-ui/core';
import { TuiInputModule, TuiTagModule } from '@taiga-ui/kit';
import { OrganizationsGridComponent } from './components/organizations-grid/organizations-grid.component';
import { NewOrganizationDialogComponent } from './components/new-organization-dialog/new-organization-dialog.component';
import { UserEditComponent } from '../shared/components/user-edit/user-edit.component';
import { TuiAutoFocusModule } from '@taiga-ui/cdk';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AdminComponent,
    UsersGridComponent,
    OrganizationsGridComponent,
    NewOrganizationDialogComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    TuiTableModule,
    TuiLinkModule,
    TuiTagModule,
    TuiButtonModule,
    TuiAutoFocusModule,
    TuiTextfieldControllerModule,
    TuiInputModule,
    FormsModule,
    TuiTablePaginationModule,
  ],
  entryComponents: [NewOrganizationDialogComponent, UserEditComponent],
})
export class AdminModule {}
