import { Component, Inject, OnInit } from '@angular/core';
import { POLYMORPHEUS_CONTEXT } from '@tinkoff/ng-polymorpheus';
import { TuiDialogContext } from '@taiga-ui/core';
import { OrganizationCreate } from '../../models/organization-create.model';

@Component({
  selector: 'app-new-organization-dialog',
  templateUrl: './new-organization-dialog.component.html',
  styleUrls: ['./new-organization-dialog.component.less'],
})
export class NewOrganizationDialogComponent implements OnInit {
  public name: string = '';

  constructor(
    @Inject(POLYMORPHEUS_CONTEXT)
    private readonly context: TuiDialogContext<OrganizationCreate>
  ) {}

  ngOnInit(): void {}

  ok() {
    this.context.completeWith({ name: this.name });
  }
}
