import { Component, Inject, Injector, OnInit } from '@angular/core';
import { UserModel } from '../../../shared/models/user.model';
import { AdminService } from '../../services/admin.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TuiDialogService } from '@taiga-ui/core';
import { PolymorpheusComponent } from '@tinkoff/ng-polymorpheus';
import { UserEditComponent } from '../../../shared/components/user-edit/user-edit.component';
import { OrganizationShort } from '../../models/organization-short.model';
import { map, of, switchMap } from 'rxjs';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { NotificationService } from '../../../shared/services/notification.service';

@UntilDestroy()
@Component({
  selector: 'app-users-grid',
  templateUrl: './users-grid.component.html',
  styleUrls: ['./users-grid.component.less'],
})
export class UsersGridComponent implements OnInit {
  get count(): number {
    return this._count;
  }

  set count(value: number) {
    this._count = value;
    this.getPaged();
  }
  get page(): number {
    return this._page - 1;
  }

  set page(value: number) {
    this._page = value + 1;
    this.getPaged();
  }
  readonly columns = ['name', 'roles', 'email', 'organization', 'actions'];
  users: UserModel[] = [];
  private _page: number = 1;
  private _count: number = 10;
  total: number = 100;

  constructor(
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
    @Inject(Injector) private readonly injector: Injector,
    public adminService: AdminService,
    private confirmService: ConfirmService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.getPaged();
  }

  private getPaged(
    page: number = this._page,
    count: number = this._count
  ): void {
    this._page = page;
    this._count = count;

    this.adminService
      .getUsersPaged({ page, count })
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.total = res.totalCount;
        this.users = res.items;
      });
  }

  createNew() {
    const dialog = this.dialogService.open<UserModel>(
      new PolymorpheusComponent(UserEditComponent, this.injector),
      {
        data: { isEdit: false, isAdmin: true },
        dismissible: true,
        label: 'Создание пользователя',
      }
    );

    dialog.subscribe({
      next: (data) => {
        this.adminService
          .createUser(data)
          .pipe(untilDestroyed(this))
          .subscribe((_) => {
            this.notificationService.showSuccess('Пользователь создан');
            this.getPaged();
          });
      },
      complete: () => {
        console.info('Dialog closed');
      },
    });
  }

  edit(item: UserModel) {
    const dialog = this.dialogService.open<UserModel>(
      new PolymorpheusComponent(UserEditComponent, this.injector),
      {
        data: { user: item, isEdit: true, isAdmin: true },
        dismissible: true,
        label: 'Редактирование пользователя',
      }
    );

    dialog.subscribe({
      next: (data) => {
        data.id = item.id;
        this.adminService
          .updateUser(data)
          .pipe(untilDestroyed(this))
          .subscribe((_) => {
            this.notificationService.showSuccess('Данные обновлены');
            this.getPaged();
          });
      },
      complete: () => {
        console.info('Dialog closed');
      },
    });
  }

  remove(item: UserModel) {
    this.confirmService
      .show(
        'Удаление пользователя',
        `Вы действительно хотите удалить пользователя "${
          item.firstName + ' ' + item.lastName
        }"?`
      )
      .pipe(
        switchMap((res) => {
          if (res) {
            return this.adminService.deleteUser(item.id).pipe(map((v) => true));
          }
          return of(false);
        }),
        untilDestroyed(this)
      )
      .subscribe((res) => {
        if (!res) {
          this.notificationService.showError('Вы отменили удаление');
          return;
        }

        this.getPaged();
        this.notificationService.showSuccess(
          `Пользователь "${item.firstName + ' ' + item.lastName}" удален`
        );
      });
  }
}
