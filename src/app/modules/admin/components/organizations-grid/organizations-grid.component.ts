import { Component, Inject, Injector, OnInit } from '@angular/core';
import { UserModel } from '../../../shared/models/user.model';
import { TuiDialogService } from '@taiga-ui/core';
import { AdminService } from '../../services/admin.service';
import { PolymorpheusComponent } from '@tinkoff/ng-polymorpheus';
import { UserEditComponent } from '../../../shared/components/user-edit/user-edit.component';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { NewOrganizationDialogComponent } from '../new-organization-dialog/new-organization-dialog.component';
import { OrganizationCreate } from '../../models/organization-create.model';
import { OrganizationShort } from '../../models/organization-short.model';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { iif, map, of, switchMap } from 'rxjs';
import { NotificationService } from '../../../shared/services/notification.service';

@UntilDestroy()
@Component({
  selector: 'app-organizations-grid',
  templateUrl: './organizations-grid.component.html',
  styleUrls: ['./organizations-grid.component.less'],
})
export class OrganizationsGridComponent implements OnInit {
  get count(): number {
    return this._count;
  }

  set count(value: number) {
    this._count = value;
    this.getPaged();
  }
  get page(): number {
    return this._page - 1;
  }

  set page(value: number) {
    this._page = value + 1;
    this.getPaged();
  }
  readonly columns = ['name', 'employeesCount', 'salePointsCount', 'actions'];
  public organizations!: OrganizationShort[];
  private _page: number = 1;
  private _count: number = 10;
  total: number = 100;

  constructor(
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
    @Inject(Injector) private readonly injector: Injector,
    public adminService: AdminService,
    private confirmService: ConfirmService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.getPaged();
  }

  private getPaged(
    page: number = this._page,
    count: number = this._count
  ): void {
    this._page = page;
    this._count = count;

    this.adminService
      .getOrganizationsPaged({ page, count })
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.total = res.totalCount;
        this.organizations = res.items;
      });
  }

  showAddDialog() {
    const dialog = this.dialogService.open<OrganizationCreate>(
      new PolymorpheusComponent(NewOrganizationDialogComponent, this.injector),
      {
        dismissible: true,
        label: 'Создание организации',
      }
    );

    dialog.subscribe({
      next: (data) => {
        this.adminService
          .createOrganization(data)
          .pipe(untilDestroyed(this))
          .subscribe((_) => {
            this.getPaged();
          });
      },
      complete: () => {
        console.info('Dialog closed');
      },
    });
  }

  remove(item: OrganizationShort) {
    this.confirmService
      .show(
        'Удаление организации',
        `Вы действительно хотите удалить организацию "${item.name}"?`
      )
      .pipe(
        switchMap((res) => {
          if (res) {
            return this.adminService
              .deleteOrganization(item.id)
              .pipe(map((v) => true));
          }
          return of(false);
        }),
        untilDestroyed(this)
      )
      .subscribe((res) => {
        if (!res) {
          this.notificationService.showError('Вы отменили удаление');
          return;
        }

        this.getPaged();
        this.notificationService.showSuccess(
          `Организация "${item.name}" удалена`
        );
      });
  }
}
