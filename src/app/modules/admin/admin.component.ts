import { Component, OnInit } from '@angular/core';
import { SidebarRouteData } from '../shared/components/sidebar-admin/sidebar-admin.component';

@Component({
  selector: 'app-distributor',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.less'],
})
export class AdminComponent implements OnInit {
  routes = [
    new SidebarRouteData('Все пользователи', '/lk/admin/users'),
    new SidebarRouteData('Все организации', '/lk/admin/organizations'),
  ];
  constructor() {}

  ngOnInit(): void {}
}
