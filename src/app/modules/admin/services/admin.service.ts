import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PagedRequest } from '../../shared/models/paged-request.model';
import { UserModel } from '../../shared/models/user.model';
import { Paged } from '../../shared/models/paged.model';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { OrganizationShort } from '../models/organization-short.model';
import { OrganizationCreate } from '../models/organization-create.model';

@Injectable({
  providedIn: 'root',
})
export class AdminService {
  constructor(private httpClient: HttpClient) {}

  public getUsersPaged(model: PagedRequest): Observable<Paged<UserModel>> {
    return this.httpClient.post<Paged<UserModel>>(
      `${environment.apiUrl}/admin/users/getPaged`,
      model
    );
  }

  createUser(model: UserModel) {
    return this.httpClient.post<string>(
      `${environment.apiUrl}/admin/users/create`,
      model
    );
  }

  updateUser(model: UserModel) {
    return this.httpClient.post<string>(
      `${environment.apiUrl}/admin/users/update`,
      model
    );
  }

  deleteUser(id: string): Observable<void> {
    return this.httpClient.delete<void>(
      `${environment.apiUrl}/admin/users/delete?id=${id}`
    );
  }

  public getOrganizationsPaged(
    model: PagedRequest
  ): Observable<Paged<OrganizationShort>> {
    return this.httpClient.post<Paged<OrganizationShort>>(
      `${environment.apiUrl}/admin/organizations/getPaged`,
      model
    );
  }

  createOrganization(model: OrganizationCreate) {
    return this.httpClient.post<string>(
      `${environment.apiUrl}/admin/organizations/create`,
      model
    );
  }

  deleteOrganization(id: string): Observable<void> {
    return this.httpClient.delete<void>(
      `${environment.apiUrl}/admin/organizations/delete?id=${id}`
    );
  }
}
