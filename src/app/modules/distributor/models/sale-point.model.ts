export class SalePoint {
  public id!: string;
  public lat!: number;
  public lon!: number;
  public name!: string;
  public address!: string;
  public organizationId!: string;
  public organizationName!: string;
}
