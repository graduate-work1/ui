import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagedRequest } from '../../shared/models/paged-request.model';
import { Observable } from 'rxjs';
import { Paged } from '../../shared/models/paged.model';
import { UserModel } from '../../shared/models/user.model';
import { environment } from '../../../../environments/environment';
import { SalePoint } from '../models/sale-point.model';
import { OrganizationProductDto } from '../../shared/models/organization-product-dto';

@Injectable({
  providedIn: 'root',
})
export class DistributorService {
  constructor(private httpClient: HttpClient) {}

  public getUsersPaged(model: PagedRequest): Observable<Paged<UserModel>> {
    return this.httpClient.post<Paged<UserModel>>(
      `${environment.apiUrl}/distributor/users/getPaged`,
      model
    );
  }

  createUser(model: UserModel) {
    return this.httpClient.post<string>(
      `${environment.apiUrl}/distributor/users/create`,
      model
    );
  }

  updateUser(model: UserModel) {
    return this.httpClient.post<string>(
      `${environment.apiUrl}/distributor/users/update`,
      model
    );
  }

  deleteUser(id: string): Observable<void> {
    return this.httpClient.delete<void>(
      `${environment.apiUrl}/distributor/users/delete?id=${id}`
    );
  }

  public getSalePointsPaged(model: PagedRequest): Observable<Paged<SalePoint>> {
    return this.httpClient.post<Paged<SalePoint>>(
      `${environment.apiUrl}/distributor/salepoints/getPaged`,
      model
    );
  }

  createSalePoint(model: SalePoint) {
    return this.httpClient.post<string>(
      `${environment.apiUrl}/distributor/salepoints/create`,
      model
    );
  }

  updateSalePoint(model: SalePoint) {
    return this.httpClient.post<string>(
      `${environment.apiUrl}/distributor/salepoints/update`,
      model
    );
  }

  deleteSalePoint(id: string): Observable<void> {
    return this.httpClient.delete<void>(
      `${environment.apiUrl}/distributor/salepoints/delete?id=${id}`
    );
  }

  importCsvProducts(file: File): Observable<void> {
    const formData = new FormData();
    formData.append('file', file, file.name);

    return this.httpClient.post<void>(
      `${environment.apiUrl}/distributor/products/importCsv`,
      formData
    );
  }

  public getProductsPaged(
    model: PagedRequest
  ): Observable<Paged<OrganizationProductDto>> {
    return this.httpClient.post<Paged<OrganizationProductDto>>(
      `${environment.apiUrl}/distributor/products/getPaged`,
      model
    );
  }

  deleteOrgProduct(id: string): Observable<void> {
    return this.httpClient.delete<void>(
      `${environment.apiUrl}/distributor/products/delete?id=${id}`
    );
  }
}
