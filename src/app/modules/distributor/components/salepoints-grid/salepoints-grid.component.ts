import { Component, Inject, Injector, OnInit } from '@angular/core';
import { UserModel } from '../../../shared/models/user.model';
import { TuiDialogService } from '@taiga-ui/core';
import { DistributorService } from '../../services/distributor.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { NotificationService } from '../../../shared/services/notification.service';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import { PolymorpheusComponent } from '@tinkoff/ng-polymorpheus';
import { UserEditComponent } from '../../../shared/components/user-edit/user-edit.component';
import { map, of, switchMap } from 'rxjs';
import { SalePoint } from '../../models/sale-point.model';
import {SalepointAddComponent} from "../salepoint-add/salepoint-add.component";

@UntilDestroy()
@Component({
  selector: 'app-salepoints-grid',
  templateUrl: './salepoints-grid.component.html',
  styleUrls: ['./salepoints-grid.component.less'],
})
export class SalepointsGridComponent implements OnInit {
  get count(): number {
    return this._count;
  }

  set count(value: number) {
    this._count = value;
    this.getPaged();
  }
  get page(): number {
    return this._page - 1;
  }

  set page(value: number) {
    this._page = value + 1;
    this.getPaged();
  }
  readonly columns = ['name', 'address', 'actions'];
  salePoints: SalePoint[] = [];
  private _page: number = 1;
  private _count: number = 10;
  total: number = 100;

  constructor(
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
    @Inject(Injector) private readonly injector: Injector,
    public distributorService: DistributorService,
    private confirmService: ConfirmService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.getPaged();
  }

  private getPaged(
    page: number = this._page,
    count: number = this._count
  ): void {
    this._page = page;
    this._count = count;

    this.distributorService
      .getSalePointsPaged({ page, count })
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.total = res.totalCount;
        this.salePoints = res.items;
      });
  }

  createNew() {
    const dialog = this.dialogService.open<SalePoint>(
      new PolymorpheusComponent(SalepointAddComponent, this.injector),
      {
        data: { isEdit: false },
        dismissible: true,
        label: 'Добавление точки продажи',
        size: 'l'
      }
    );

    dialog.subscribe({
      next: (data) => {
        this.distributorService
          .createSalePoint(data)
          .pipe(untilDestroyed(this))
          .subscribe((_) => {
            this.notificationService.showSuccess(`Точка по адресу "${data.address}" создана`);
            this.getPaged();
          });
      },
      complete: () => {
        console.info('Dialog closed');
      },
    });
  }

  edit(item: SalePoint) {
    const dialog = this.dialogService.open<SalePoint>(
      new PolymorpheusComponent(SalepointAddComponent, this.injector),
      {
        data: { isEdit: true, data: item },
        dismissible: true,
        label: 'Изменение точки продажи',
        size: 'l'
      }
    );

    dialog.subscribe({
      next: (data) => {
        this.distributorService
          .updateSalePoint(data)
          .pipe(untilDestroyed(this))
          .subscribe((_) => {
            this.notificationService.showSuccess(`Точка по адресу "${data.address}" изменена`);
            this.getPaged();
          });
      },
      complete: () => {
        console.info('Dialog closed');
      },
    });
  }

  remove(item: SalePoint) {
    this.confirmService
      .show(
        'Удаление точки',
        `Вы действительно хотите удалить точку "${item.name}"?`
      )
      .pipe(
        switchMap((res) => {
          if (res) {
            return this.distributorService
              .deleteSalePoint(item.id)
              .pipe(map((v) => true));
          }
          return of(false);
        }),
        untilDestroyed(this)
      )
      .subscribe((res) => {
        if (!res) {
          this.notificationService.showError('Вы отменили удаление');
          return;
        }

        this.getPaged();
        this.notificationService.showSuccess(`Точка "${item.name}" удалена`);
      });
  }
}
