import { Component, Inject, Injector, OnInit } from '@angular/core';
import { SalePoint } from '../../models/sale-point.model';
import { TuiDialogService } from '@taiga-ui/core';
import { DistributorService } from '../../services/distributor.service';
import { ConfirmService } from '../../../shared/services/confirm.service';
import { NotificationService } from '../../../shared/services/notification.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { PolymorpheusComponent } from '@tinkoff/ng-polymorpheus';
import { SalepointAddComponent } from '../salepoint-add/salepoint-add.component';
import { map, of, switchMap } from 'rxjs';
import { FormControl } from '@angular/forms';
import { OrganizationProductDto } from '../../../shared/models/organization-product-dto';

@UntilDestroy()
@Component({
  selector: 'app-products-grid',
  templateUrl: './products-grid.component.html',
  styleUrls: ['./products-grid.component.less'],
})
export class ProductsGridComponent implements OnInit {
  get count(): number {
    return this._count;
  }

  set count(value: number) {
    this._count = value;
    this.getPaged();
  }
  get page(): number {
    return this._page - 1;
  }

  set page(value: number) {
    this._page = value + 1;
    this.getPaged();
  }

  public isFilesOpen = false;
  public filesControl = new FormControl();
  readonly columns = ['name', 'price', 'availability', 'barCode', 'actions'];
  products: OrganizationProductDto[] = [];
  private _page: number = 1;
  private _count: number = 10;
  total: number = 100;

  constructor(
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
    @Inject(Injector) private readonly injector: Injector,
    public distributorService: DistributorService,
    private confirmService: ConfirmService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.getPaged();
    this.filesControl.valueChanges
      .pipe(untilDestroyed(this))
      .subscribe((file) => {
        this.distributorService.importCsvProducts(file).subscribe((_) => {
          this.notificationService.showSuccess('Данные успешно загружены');

          this.getPaged();
        });
      });
  }

  private getPaged(
    page: number = this._page,
    count: number = this._count
  ): void {
    this._page = page;
    this._count = count;

    this.distributorService
      .getProductsPaged({ page, count })
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.total = res.totalCount;
        this.products = res.items;
      });
  }

  remove(item: OrganizationProductDto) {
    this.confirmService
      .show(
        'Удаление продукта',
        `Вы действительно хотите удалить продукт с названием "${item.product.name}"?`
      )
      .pipe(
        switchMap((res) => {
          if (res) {
            return this.distributorService
              .deleteOrgProduct(item.id)
              .pipe(map((v) => true));
          }
          return of(false);
        }),
        untilDestroyed(this)
      )
      .subscribe((res) => {
        if (!res) {
          this.notificationService.showError('Вы отменили удаление');
          return;
        }

        this.getPaged();
        this.notificationService.showSuccess(
          `Продукт "${item.product.name}" удален`
        );
      });
  }
}
