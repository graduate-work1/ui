import { Component, Inject, OnInit } from '@angular/core';
import { TuiDialogContext, TuiDialogService } from '@taiga-ui/core';
import { POLYMORPHEUS_CONTEXT } from '@tinkoff/ng-polymorpheus';
import { SalePoint } from '../../models/sale-point.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { YaReadyEvent } from 'angular8-yandex-maps';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-salepoint-add',
  templateUrl: './salepoint-add.component.html',
  styleUrls: ['./salepoint-add.component.less'],
})
export class SalepointAddComponent implements OnInit {
  public form!: FormGroup;
  public map!: ymaps.Map;
  public geoObject!: ymaps.GeoObject;
  public isEdit: boolean = false;

  constructor(
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
    @Inject(POLYMORPHEUS_CONTEXT)
    private readonly context: TuiDialogContext<
      SalePoint,
      { isEdit: boolean; data: SalePoint }
    >,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.isEdit = this.context.data.isEdit;

    const formValue = this.context.data.data;

    this.form = this.fb.group({
      id: [formValue?.id || null],
      lat: [formValue?.lat || null, Validators.compose([Validators.required])],
      lon: [formValue?.lon || null, Validators.compose([Validators.required])],
      name: [
        formValue?.name || null,
        Validators.compose([Validators.required]),
      ],
      address: [
        formValue?.address || null,
        Validators.compose([Validators.required]),
      ],
    });
  }

  submit() {
    this.context.completeWith(this.form.getRawValue());
  }

  onMapReady(event: YaReadyEvent<ymaps.Map>): void {
    this.map = event.target;
    const map = event.target;

    ymaps.geolocation
      .get({
        provider: 'yandex',
        mapStateAutoApply: true,
      })
      .then((result) => {
        result.geoObjects.options.set('preset', 'islands#redCircleIcon');
        result.geoObjects.get(0).properties.set({
          balloonContentBody: 'Вы здесь!',
        });

        map.geoObjects.add(result.geoObjects);
      });

    ymaps.geolocation
      .get({
        provider: 'browser',
        mapStateAutoApply: true,
      })
      .then((result) => {
        result.geoObjects.options.set('preset', 'islands#blueCircleIcon');
        map.geoObjects.add(result.geoObjects);
      });

    const formValue = this.context.data.data;
    if (formValue?.lat && formValue?.lon) {
      this.geoObject = new ymaps.GeoObject({
        geometry: {
          type: 'Point', // тип геометрии - точка
          coordinates: [formValue?.lat, formValue?.lon], // координаты точки
        },
      });
      map.geoObjects.add(this.geoObject);
    }

    this.map.events.add('click', (e) => {
      if (this.geoObject) {
        map.geoObjects.remove(this.geoObject);
      }

      const coords = e.get('coords');
      this.geoObject = new ymaps.GeoObject({
        geometry: {
          type: 'Point', // тип геометрии - точка
          coordinates: coords, // координаты точки
        },
      });
      map.geoObjects.add(this.geoObject);

      ymaps.geocode(coords).then((res: any) => {
        this.form.patchValue({
          address: res.geoObjects.get(0).getAddressLine(),
        });
      });

      this.form.patchValue({
        lat: coords[0],
        lon: coords[1],
      });
    });
  }
}
