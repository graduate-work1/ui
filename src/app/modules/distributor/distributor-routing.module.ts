import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuard } from '../shared/guards/role-guard.service';
import { UserRoleEnum } from '../shared/models/user-role.enum';
import { DistributorUsersGridComponent } from './components/users-grid/distributor-users-grid.component';
import { DistributorComponent } from './distributor.component';
import { SalepointsGridComponent } from './components/salepoints-grid/salepoints-grid.component';
import {ProductsGridComponent} from "./components/products-grid/products-grid.component";

const routes: Routes = [
  {
    path: '',
    component: DistributorComponent,
    canActivate: [RoleGuard],
    data: {
      role: UserRoleEnum.Distributor,
    },
    children: [
      { path: 'users', component: DistributorUsersGridComponent },
      { path: 'salepoints', component: SalepointsGridComponent },
      { path: 'products', component: ProductsGridComponent },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'users',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DistributorRoutingModule {}
