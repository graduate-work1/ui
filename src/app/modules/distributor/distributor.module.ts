import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DistributorRoutingModule } from './distributor-routing.module';
import { DistributorComponent } from './distributor.component';
import { DistributorUsersGridComponent } from './components/users-grid/distributor-users-grid.component';
import { AdminRoutingModule } from '../admin/admin-routing.module';
import { SharedModule } from '../shared/shared.module';
import {
  TuiTableModule,
  TuiTablePaginationModule,
} from '@taiga-ui/addon-table';
import {
  TuiButtonModule,
  TuiLinkModule,
  TuiTextfieldControllerModule,
} from '@taiga-ui/core';
import {TuiFilesModule, TuiInputFilesModule, TuiInputModule, TuiTagModule} from '@taiga-ui/kit';
import { TuiAutoFocusModule } from '@taiga-ui/cdk';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UserEditComponent } from '../shared/components/user-edit/user-edit.component';
import { SalepointsGridComponent } from './components/salepoints-grid/salepoints-grid.component';
import { SalepointAddComponent } from './components/salepoint-add/salepoint-add.component';
import {AngularYandexMapsModule} from "angular8-yandex-maps";
import { ProductsGridComponent } from './components/products-grid/products-grid.component';

@NgModule({
  declarations: [DistributorComponent, DistributorUsersGridComponent, SalepointsGridComponent, SalepointAddComponent, ProductsGridComponent],
  imports: [
    CommonModule,
    DistributorRoutingModule,
    SharedModule,
    TuiTableModule,
    TuiLinkModule,
    TuiTagModule,
    TuiButtonModule,
    TuiAutoFocusModule,
    TuiTextfieldControllerModule,
    TuiInputModule,
    FormsModule,
    TuiTablePaginationModule,
    AngularYandexMapsModule.forRoot({
      apikey: '87b90239-1e21-40e5-923f-66354c2aca09'
    }),
    ReactiveFormsModule,
    TuiInputFilesModule,
    TuiFilesModule,
  ],
  entryComponents: [UserEditComponent, SalepointAddComponent],
})
export class DistributorModule {}
