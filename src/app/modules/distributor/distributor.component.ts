import { Component, OnInit } from '@angular/core';
import { SidebarRouteData } from '../shared/components/sidebar-admin/sidebar-admin.component';

@Component({
  selector: 'app-distributor',
  templateUrl: './distributor.component.html',
  styleUrls: ['./distributor.component.less'],
})
export class DistributorComponent implements OnInit {
  routes = [
    new SidebarRouteData('Пользователи организации', '/lk/distributor/users'),
    new SidebarRouteData('Точки продажи', '/lk/distributor/salepoints'),
    new SidebarRouteData('Представленные товары', '/lk/distributor/products'),
  ];
  constructor() {}

  ngOnInit(): void {}
}
