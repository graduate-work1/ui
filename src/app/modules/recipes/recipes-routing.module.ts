import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipesNewComponent } from './components/recipes-new/recipes-new.component';
import { RecipesGridComponent } from './components/recipes-grid/recipes-grid.component';
import { RecipesViewComponent } from './components/recipes-view/recipes-view.component';
import { ErrorComponent } from '../../components/error/error.component';

const routes: Routes = [
  { path: 'new', component: RecipesNewComponent },
  { path: 'all', component: RecipesGridComponent },
  { path: 'view/:id', component: RecipesViewComponent },
  { path: '', pathMatch: 'full', redirectTo: 'all' },

  {
    path: '**',
    pathMatch: 'full',
    component: ErrorComponent,
    data: {
      error: 404,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecipesRoutingModule {}
