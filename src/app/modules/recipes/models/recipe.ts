import { UserShort } from '../../recipe-shared/models/user-short.model';
import { DifficultyEnum } from './difficulty.enum';
import { UserModel } from '../../shared/models/user.model';

export interface Recipe {
  id: string;
  title: string;
  description: string;
  imageUrl: string;
  userId: string;
  user: UserModel;
  additional: string;
  steps: RecipeStep[];
  ingredientBlocks: IngredientBlock[];
  serves: number;
  preparationTime: number;
  preparationPlural: string;
  cookTime: number;
  cookPlural: string;
  likes: number;
  difficulty: DifficultyEnum;
}

export interface RecipeStep {
  text: string;
  imageUrl: string;
}

export interface IngredientBlock {
  title: string;
  items: IngredientItem[];
}

export interface IngredientItem {
  title: string;
  count: string;
  productId: string;
}
