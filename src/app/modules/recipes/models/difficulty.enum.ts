export enum DifficultyEnum {
  Unknown = 0,
  Newbie,
  Easy,
  Medium,
  Hard,
  Extra,
}
