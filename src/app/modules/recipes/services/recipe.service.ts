import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagedRequest } from '../../shared/models/paged-request.model';
import { Observable } from 'rxjs';
import { Paged } from '../../shared/models/paged.model';
import { UserModel } from '../../shared/models/user.model';
import { environment } from '../../../../environments/environment';
import { Recipe } from '../models/recipe';

@Injectable({
  providedIn: 'root',
})
export class RecipeService {
  constructor(private httpClient: HttpClient) {}

  public getPaged(model: any): Observable<Paged<Recipe>> {
    return this.httpClient.post<Paged<Recipe>>(
      `${environment.apiUrl}/recipes/getPaged`,
      model
    );
  }

  new(model: any) {
    return this.httpClient.post<void>(
      `${environment.apiUrl}/recipes/new`,
      model
    );
  }

  get(id: string) {
    return this.httpClient.get<Recipe>(
      `${environment.apiUrl}/recipes/getById?id=${id}`
    );
  }

  delete(id: string): Observable<void> {
    return this.httpClient.delete<void>(
      `${environment.apiUrl}/recipes/delete?id=${id}`
    );
  }
}
