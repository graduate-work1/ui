import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecipesRoutingModule } from './recipes-routing.module';
import { RecipesGridComponent } from './components/recipes-grid/recipes-grid.component';
import { RecipesNewComponent } from './components/recipes-new/recipes-new.component';
import { RecipesViewComponent } from './components/recipes-view/recipes-view.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import {
  TuiAvatarModule,
  TuiCheckboxLabeledModule,
  TuiDataListWrapperModule,
  TuiFieldErrorModule,
  TuiInputCountModule,
  TuiInputDateModule,
  TuiInputFileModule,
  TuiInputModule,
  TuiInputNumberModule,
  TuiInputPasswordModule,
  TuiInputPhoneModule,
  TuiInputSliderModule,
  TuiInputTimeModule,
  TuiRadioBlockModule,
  TuiSelectModule,
  TuiStepperModule,
  TuiTagModule,
} from '@taiga-ui/kit';
import { SharedModule } from '../shared/shared.module';
import {
  TuiButtonModule,
  TuiDataListModule,
  TuiGroupModule,
  TuiHintControllerModule,
  TuiTextfieldControllerModule,
} from '@taiga-ui/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  TuiCurrencyPipeModule,
  TuiMoneyModule,
} from '@taiga-ui/addon-commerce';
import { TuiEditorNewModule } from '@taiga-ui/addon-editor';
import { TuiLetModule } from '@taiga-ui/cdk';
import { TuiTablePaginationModule } from '@taiga-ui/addon-table';
import { RecipeSharedModule } from '../recipe-shared/recipe-shared.module';

@NgModule({
  declarations: [
    RecipesGridComponent,
    RecipesNewComponent,
    RecipesViewComponent,
  ],
  imports: [
    CommonModule,
    RecipesRoutingModule,
    AngularSvgIconModule,
    TuiAvatarModule,
    TuiTagModule,
    SharedModule,
    TuiButtonModule,
    TuiStepperModule,
    ReactiveFormsModule,
    TuiInputModule,
    TuiFieldErrorModule,
    TuiInputDateModule,
    TuiInputPasswordModule,
    TuiInputNumberModule,
    TuiCurrencyPipeModule,
    TuiInputSliderModule,
    TuiSelectModule,
    TuiDataListWrapperModule,
    TuiInputPhoneModule,
    TuiRadioBlockModule,
    TuiGroupModule,
    TuiCheckboxLabeledModule,
    TuiInputTimeModule,
    TuiMoneyModule,
    TuiTextfieldControllerModule,
    TuiHintControllerModule,
    TuiEditorNewModule,
    TuiInputFileModule,
    TuiInputCountModule,
    TuiDataListModule,
    TuiLetModule,
    TuiTablePaginationModule,
    RecipeSharedModule,
  ],
})
export class RecipesModule {}
