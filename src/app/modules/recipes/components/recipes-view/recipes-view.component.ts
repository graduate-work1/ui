import { Component, OnDestroy, OnInit } from '@angular/core';
import { DifficultyEnum } from '../../models/difficulty.enum';
import { HeaderService } from '../../../shared/services/header.service';
import { RecipeService } from '../../services/recipe.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recipes-view',
  templateUrl: './recipes-view.component.html',
  styleUrls: ['./recipes-view.component.less'],
})
export class RecipesViewComponent implements OnInit, OnDestroy {
  public recipe$ = this.recipeService.get(
    this.activatedRoute.snapshot.params['id']
  );

  mapping: { [k: string]: { [v: string]: string } } = {
    seconds: {
      one: 'секунда',
      few: 'секунды',
      many: 'секунд',
      other: 'секунд',
    },
    minutes: {
      one: 'минута',
      few: 'минуты',
      many: 'минут',
      other: 'минут',
    },
    hours: {
      one: 'час',
      few: 'часа',
      many: 'часов',
      other: 'часов',
    },
  };

  constructor(
    private headerService: HeaderService,
    private recipeService: RecipeService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.headerService.showShadow$.next(false);
    this.headerService.isWhite$.next(true);
  }

  ngOnDestroy() {
    this.headerService.showShadow$.next(true);
    this.headerService.isWhite$.next(false);
  }
}
