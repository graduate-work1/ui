import { Component, OnInit } from '@angular/core';

import { UserService } from '../../../shared/services/user.service';
import { RecipeService } from '../../services/recipe.service';
import { FormControl } from '@angular/forms';
import { debounceTime, Observable } from 'rxjs';
import { ProductShort } from '../../../shared/models/ProductShort';
import { Recipe } from '../../models/recipe';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-recipes-grid',
  templateUrl: './recipes-grid.component.html',
  styleUrls: ['./recipes-grid.component.less'],
})
export class RecipesGridComponent implements OnInit {
  public popular = {
    image:
      'https://s1.eda.ru/StaticContent/Photos/120131111615/180226214611/p_O.jpg',
    time: {
      preparationTime: 5,
      preparationPlural: 'minutes',
      cookTime: 35,
      cookPlural: 'minutes',
    },
    title: 'Брауни (brownie)',
    likes: 524,
  };

  searchControl: FormControl = new FormControl();
  categories$!: Observable<string[]>;
  get count(): number {
    return this._count;
  }

  set count(value: number) {
    this._count = value;
    this.getPaged();
  }
  get page(): number {
    return this._page - 1;
  }

  set page(value: number) {
    this._page = value + 1;
    this.getPaged();
  }
  recipes: Recipe[] = [];
  private _page: number = 1;
  private _count: number = 10;
  total: number = 100;
  text: string | null = null;
  category: string | null = null;

  constructor(
    public userService: UserService,
    public recipeService: RecipeService
  ) {}

  ngOnInit(): void {
    // this.categories$ = this.productService.getCategories();
    this.searchControl.valueChanges
      .pipe(debounceTime(200), untilDestroyed(this))
      .subscribe((search) => {
        this.text = search;
        this.getPaged();
      });
    this.getPaged();
  }

  private getPaged(
    page: number = this._page,
    count: number = this._count
  ): void {
    this._page = page;
    this._count = count;

    this.recipeService
      .getPaged({
        page,
        count,
        text: this.text,
        category: this.category,
      })
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.total = res.totalCount;
        this.recipes = res.items;
      });
  }
}
