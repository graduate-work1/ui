import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { TuiCurrency } from '@taiga-ui/addon-commerce';
import { TuiDay, TuiTime } from '@taiga-ui/cdk';
import { TuiPluralize } from '@taiga-ui/core';
import { UserShort } from '../../../recipe-shared/models/user-short.model';
import { DifficultyEnum } from '../../models/difficulty.enum';
import { IngredientBlock, RecipeStep } from '../../models/recipe';
import {
  defaultEditorExtensions,
  TUI_EDITOR_EXTENSIONS,
} from '@taiga-ui/addon-editor';
import { BehaviorSubject, debounceTime, switchMap } from 'rxjs';
import { ProductShort } from '../../../shared/models/ProductShort';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ProductService } from '../../../products/services/product.service';
import { Paged } from '../../../shared/models/paged.model';
import { NotificationService } from '../../../shared/services/notification.service';
import { RecipeService } from '../../services/recipe.service';
import { Router } from '@angular/router';
@UntilDestroy()
@Component({
  selector: 'app-recipes-new',
  templateUrl: './recipes-new.component.html',
  styleUrls: ['./recipes-new.component.less'],
  providers: [
    {
      provide: TUI_EDITOR_EXTENSIONS,
      useValue: defaultEditorExtensions,
    },
  ],
})
export class RecipesNewComponent {
  public get steps(): FormArray {
    return this.form.controls['steps'] as FormArray;
  }
  public get ingredientBlocks(): FormArray {
    return this.form.controls['ingredientBlocks'] as FormArray;
  }

  public items$ = new BehaviorSubject<ProductShort[]>([]);

  public form = this._fb.group({
    title: [null, Validators.compose([Validators.required])],
    description: [null, Validators.compose([Validators.required])],
    imageUrl: [null],
    image: [null, Validators.compose([Validators.required])],
    serves: [null, Validators.compose([Validators.required])],
    preparationTime: [1, Validators.compose([Validators.required])],
    preparationPlural: [null, Validators.compose([Validators.required])],
    cookTime: [1, Validators.compose([Validators.required])],
    cookPlural: [null, Validators.compose([Validators.required])],
    difficulty: [null, Validators.compose([Validators.required])],
    additional: [null, Validators.compose([])],
    steps: this._fb.array([]),
    ingredientBlocks: this._fb.array([]),
  });

  timePlural = ['seconds', 'minutes', 'hours'];
  difficulties: any[] = [
    DifficultyEnum.Newbie,
    DifficultyEnum.Easy,
    DifficultyEnum.Medium,
    DifficultyEnum.Hard,
    DifficultyEnum.Extra,
  ];

  mapping: { [k: string]: { [v: string]: string } } = {
    seconds: {
      one: 'Секунда',
      few: 'Секунды',
      many: 'Секунд',
      other: 'Секунд',
    },
    minutes: {
      one: 'Минута',
      few: 'Минуты',
      many: 'Минут',
      other: 'Минут',
    },
    hours: {
      one: 'Час',
      few: 'Часа',
      many: 'Часов',
      other: 'Часов',
    },
  };

  /*

export interface Recipe {
  ingredientBlocks: IngredientBlock[];

}

 */

  constructor(
    private _fb: FormBuilder,
    private _productService: ProductService,
    private _recipeService: RecipeService,
    private _router: Router,
    private _notifications: NotificationService
  ) {}

  ngOnInit() {
    this.newRecipeStep();
    this.newIngredientBlock();

    this.form.controls['image'].valueChanges
      .pipe(untilDestroyed(this))
      .subscribe((image) => {
        const reader = new FileReader();
        reader.addEventListener(
          'load',
          () => {
            // convert image file to base64 string
            this.form.controls['imageUrl'].patchValue(reader.result, {
              emitEvent: false,
            });
          },
          false
        );
        reader.readAsDataURL(image);
      });
  }

  newRecipeStep() {
    const step = this._fb.group({
      text: [null, Validators.compose([Validators.required])],
      image: [null, Validators.compose([])],
      imageUrl: [null, Validators.compose([])],
    });

    step.controls['image'].valueChanges
      .pipe(untilDestroyed(this))
      .subscribe((image) => {
        const reader = new FileReader();
        reader.addEventListener(
          'load',
          () => {
            // convert image file to base64 string
            step.controls['imageUrl'].patchValue(reader.result, {
              emitEvent: false,
            });
          },
          false
        );
        reader.readAsDataURL(image);
      });

    this.steps.push(step);
  }

  getStepByIndex(i: number): any {
    return this.steps.at(i) as any;
  }

  deleteRecipeStep(i: number): void {
    this.steps.removeAt(i);
  }

  newIngredientBlock() {
    const block = this._fb.group({
      title: [null, Validators.compose([Validators.required])],
      items: this._fb.array([]),
    });

    this.ingredientBlocks.push(block);
    this.newIngredientItem(block);
  }

  getIngredientBlockByIndex(i: number): any {
    return this.ingredientBlocks.at(i) as any;
  }

  deleteIngredientBlock(i: number): void {
    this.ingredientBlocks.removeAt(i);
  }

  public getIngredientItems(block: any): FormArray {
    return block.controls['items'] as FormArray;
  }

  newIngredientItem(block: any) {
    const item = this._fb.group({
      title: [null, Validators.compose([Validators.required])],
      count: [null, Validators.compose([Validators.required])],
      productId: [null],
      productPriceStart: [null],
    });
    item.controls['title'].valueChanges
      .pipe(
        debounceTime(300),
        switchMap((res: string) => {
          return this._productService.getProductsPaged({
            page: 1,
            count: 10,
            text: res,
            category: null,
          });
        }),
        untilDestroyed(this)
      )
      .subscribe((res: Paged<ProductShort>) => {
        this.items$.next(res.items);
      });

    (this.getIngredientItems(block) as FormArray).push(item);
  }

  getIngredientItemByIndex(block: any, i: number): any {
    return this.getIngredientItems(block).at(i) as any;
  }

  deleteIngredientItem(block: any, i: number): void {
    this.getIngredientItems(block).removeAt(i);
  }

  selectIngredient(block: any, i: number, product: ProductShort) {
    this.getIngredientItemByIndex(block, i).patchValue({
      title: product.name,
      productId: product.id,
      productPriceStart: product.priceStart,
    });
  }

  publish() {
    this.form.markAllAsTouched();
    this.form.updateValueAndValidity();

    if (this.form.invalid) {
      this._notifications.showError('Проверьте заполнение формы');
      return;
    }

    const model = this.form.getRawValue();
    delete model.image;
    model.steps.forEach((step: { image: any }) => {
      delete step.image;
    });

    this._recipeService
      .new(model)
      .pipe(untilDestroyed(this))
      .subscribe((_) => {
        this._router.navigateByUrl('/recipes/all');
        this._notifications.showSuccess('Рецепт опубликован');
      });
  }
}
