import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductGridComponent } from './components/product-grid/product-grid.component';
import { ProductsComponent } from './products.component';
import { TuiInputModule, TuiLineClampModule } from '@taiga-ui/kit';
import { ReactiveFormsModule } from '@angular/forms';
import { TuiTablePaginationModule } from '@taiga-ui/addon-table';
import { TuiMoneyModule } from '@taiga-ui/addon-commerce';
import { ProductViewComponent } from './components/product-view/product-view.component';
import { AngularYandexMapsModule } from 'angular8-yandex-maps';

@NgModule({
  declarations: [ProductGridComponent, ProductsComponent, ProductViewComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    TuiInputModule,
    ReactiveFormsModule,
    TuiTablePaginationModule,
    TuiMoneyModule,
    TuiLineClampModule,
    AngularYandexMapsModule,
  ],
})
export class ProductsModule {}
