import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Observable, tap } from 'rxjs';
import { ProductService } from '../../services/product.service';
import { YaReadyEvent } from 'angular8-yandex-maps';
import { SalePoint } from '../../../distributor/models/sale-point.model';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.less'],
})
export class ProductViewComponent {
  public id!: string;
  public product$!: Observable<any>;

  public map!: ymaps.Map;
  public geoMe!: ymaps.IGeoObject<ymaps.IGeometry>;
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _productService: ProductService,
    private _cdr: ChangeDetectorRef
  ) {}

  add(): void {
    this.id = this._activatedRoute.snapshot.params['id'];
    this.product$ = this._productService.getById(this.id).pipe(
      map((prod) => {
        prod.organizations.forEach((org: any) => {
          let d = Number.MAX_SAFE_INTEGER;
          org.salePoints.forEach((sp: SalePoint) => {
            const geoObject = new ymaps.GeoObject({
              geometry: {
                type: 'Point', // тип геометрии - точка
                coordinates: [sp.lat, sp.lon], // координаты точки
              },
            });
            const dist = (ymaps as any).coordSystem.geo.getDistance(
              [sp.lat, sp.lon],
              (this.geoMe.geometry as any).getCoordinates()
            );
            if (dist < d) {
              d = dist;
            }
            geoObject.properties.set({
              balloonContentBody: `<b>${org.name}</b><br/>Название: ${
                sp.name
              }<br/>Адрес: ${sp.address}<br/>Дистанция: ${dist.toFixed(2)} м`,
            });
            this.map.geoObjects.add(geoObject);
          });

          org.salePointDistance = d.toFixed(2);
        });
        return prod;
      }),
      tap((_) => {
        setTimeout(() => {
          this._cdr.detectChanges();
        });
      })
    );
    this._cdr.detectChanges();
  }

  onMapReady(event: YaReadyEvent<ymaps.Map>): void {
    this.map = event.target;
    const map = event.target;
    /**
     * Comparing the position calculated from the user's IP address
     * and the position detected using the browser.
     */
    ymaps.geolocation
      .get({
        provider: 'yandex',
        mapStateAutoApply: true,
      })
      .then((result) => {
        // We'll mark the position calculated by IP in red.
        result.geoObjects.options.set('preset', 'islands#redCircleIcon');
        result.geoObjects.get(0).properties.set({
          balloonContentBody: 'Вы здесь!',
        });

        map.geoObjects.add(result.geoObjects);
        this.geoMe = result.geoObjects.get(0);
        this.add();
      });

    ymaps.geolocation
      .get({
        provider: 'browser',
        mapStateAutoApply: true,
      })
      .then((result) => {
        /**
         * We'll mark the position obtained through the browser in blue.
         * If the browser does not support this functionality, the placemark will not be added to the map.
         */
        result.geoObjects.options.set('preset', 'islands#blueCircleIcon');
        map.geoObjects.add(result.geoObjects);
      });
  }
}
