import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { debounceTime, Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { OrganizationProductDto } from '../../../shared/models/organization-product-dto';
import { ProductShort } from '../../../shared/models/ProductShort';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-product-grid',
  templateUrl: './product-grid.component.html',
  styleUrls: ['./product-grid.component.less'],
})
export class ProductGridComponent implements OnInit {
  searchControl: FormControl = new FormControl();
  categories$!: Observable<string[]>;
  get count(): number {
    return this._count;
  }

  set count(value: number) {
    this._count = value;
    this.getPaged();
  }
  get page(): number {
    return this._page - 1;
  }

  set page(value: number) {
    this._page = value + 1;
    this.getPaged();
  }
  products: ProductShort[] = [];
  private _page: number = 1;
  private _count: number = 10;
  total: number = 100;
  text: string | null = null;
  category: string | null = null;

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.categories$ = this.productService.getCategories();
    this.searchControl.valueChanges
      .pipe(debounceTime(200), untilDestroyed(this))
      .subscribe((search) => {
        this.text = search;
        this.getPaged();
      });
    this.getPaged();
  }

  private getPaged(
    page: number = this._page,
    count: number = this._count
  ): void {
    this._page = page;
    this._count = count;

    this.productService
      .getProductsPaged({
        page,
        count,
        text: this.text,
        category: this.category,
      })
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.total = res.totalCount;
        this.products = res.items;
      });
  }

  setCategory(category: string | null) {
    this.category = category;
    this.getPaged();
  }

  clamp(description: string) {
    return description.slice(0, 150) + '...';
  }
}
