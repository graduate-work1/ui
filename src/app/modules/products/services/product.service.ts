import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagedRequest } from '../../shared/models/paged-request.model';
import { Observable } from 'rxjs';
import { Paged } from '../../shared/models/paged.model';
import { environment } from '../../../../environments/environment';
import { ProductShort } from '../../shared/models/ProductShort';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private httpClient: HttpClient) {}

  public getProductsPaged(model: any): Observable<Paged<ProductShort>> {
    return this.httpClient.post<Paged<ProductShort>>(
      `${environment.apiUrl}/product/getPaged`,
      model
    );
  }
  public getCategories(): Observable<string[]> {
    return this.httpClient.get<string[]>(
      `${environment.apiUrl}/product/getCategories`
    );
  }
  public getById(id: string): Observable<any> {
    return this.httpClient.get<any>(
      `${environment.apiUrl}/product/getById?id=${id}`
    );
  }
}
