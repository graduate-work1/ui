import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less'],
})
export class RegisterComponent implements OnInit {
  registerForm = this.fb.group({
    email: ['', Validators.compose([Validators.required, Validators.email])],
    firstName: ['', Validators.compose([Validators.required])],
    lastName: ['', Validators.compose([Validators.required])],
    userName: ['', Validators.compose([Validators.required])],
    password: [
      '',
      Validators.compose([Validators.required, Validators.minLength(8)]),
    ],
    addpassword: ['', Validators.compose([Validators.required])],
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  register() {
    this.registerForm.updateValueAndValidity();
    if (
      this.registerForm.controls['addpassword'].value !==
      this.registerForm.controls['password'].value
    ) {
      this.registerForm.controls['addpassword'].setErrors({ error: true });
    }
    if (this.registerForm.invalid) {
      this.registerForm.markAllAsTouched();
      return;
    }

    this.authService
      .register(this.registerForm.getRawValue())
      .pipe(untilDestroyed(this))
      .subscribe((_) => {
        this.router.navigateByUrl('/').then();
      });
  }
}
