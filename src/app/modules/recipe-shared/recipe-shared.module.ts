import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipeCardComponent } from './components/recipe-card/recipe-card.component';
import { RecipePreviewComponent } from './components/recipe-preview/recipe-preview.component';
import { RecipeCategoryComponent } from './components/recipe-category/recipe-category.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { RouterModule } from '@angular/router';
import { ProductCardComponent } from './components/product-card/product-card.component';

@NgModule({
  declarations: [
    RecipeCardComponent,
    RecipePreviewComponent,
    RecipeCategoryComponent,
    ProductCardComponent,
  ],
  imports: [CommonModule, AngularSvgIconModule, RouterModule],
  exports: [RecipeCardComponent, RecipePreviewComponent, ProductCardComponent],
})
export class RecipeSharedModule {}
