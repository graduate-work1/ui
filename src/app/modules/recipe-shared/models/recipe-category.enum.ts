export enum RecipeCategoryEnum {
  Unknown,
  Breakfasts,
  Snacks,
  Drinks,
  MainDishes,
  Salads,
  Soups,
  Desserts,
}
