import { Ingredient } from './ingredient.model';

export interface IngredientsBlock {
  title: string;
  items: Ingredient[];
}
