import { RecipeCategoryEnum } from './recipe-category.enum';
import { UserShort } from './user-short.model';

export class RecipeShort {
  public title!: string;
  public image!: string;
  public categoryId!: RecipeCategoryEnum;
  public description!: string;
  public user!: UserShort;
}
