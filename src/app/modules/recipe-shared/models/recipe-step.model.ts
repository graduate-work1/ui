export interface RecipeStep {
  text: string;
  image?: string;
}
