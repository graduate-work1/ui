export interface Ingredient {
  title: string;
  count: string;
  productId?: string;
}
