import { DifficultyEnum } from '../../recipes/models/difficulty.enum';
import { RecipeShort } from './recipe-short.model';
import { RecipeStep } from './recipe-step.model';
import { IngredientsBlock } from './ingredients-block.model';
import { CookTimeModel } from '../../shared/models/cook-time.model';

export class Recipe extends RecipeShort {
  additional?: string;
  steps!: RecipeStep[];
  ingredients!: IngredientsBlock[];
  serves!: number;
  time!: CookTimeModel;
  likes!: number;
  difficulty!: DifficultyEnum;
}
