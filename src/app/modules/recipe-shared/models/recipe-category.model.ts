import { RecipeCategoryEnum } from './recipe-category.enum';

export interface RecipeCategory {
  id: RecipeCategoryEnum;
  title: string;
  icon: string;
}
