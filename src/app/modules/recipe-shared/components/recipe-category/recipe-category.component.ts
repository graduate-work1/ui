import { Component, OnInit, Input } from '@angular/core';
import { RecipeCategoryEnum } from '../../models/recipe-category.enum';
import { RecipeCategoriesService } from '../../services/recipe-categories.service';
import { RecipeCategory } from '../../models/recipe-category.model';

@Component({
  selector: 'app-recipe-category',
  templateUrl: './recipe-category.component.html',
  styleUrls: ['./recipe-category.component.less'],
})
export class RecipeCategoryComponent implements OnInit {
  @Input()
  get categoryId(): RecipeCategoryEnum {
    return this._categoryId;
  }

  set categoryId(value: RecipeCategoryEnum) {
    this._categoryId = value;
    this.category = RecipeCategoriesService.getCategory(value);
  }

  public category!: RecipeCategory;

  private _categoryId!: RecipeCategoryEnum;

  constructor() {}

  ngOnInit(): void {}
}
