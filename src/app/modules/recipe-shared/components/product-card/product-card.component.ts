import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from '../../../recipes/models/recipe';
import { ProductShort } from '../../../shared/models/ProductShort';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.less'],
})
export class ProductCardComponent implements OnInit {
  @Input() public product!: ProductShort;

  constructor() {}

  ngOnInit(): void {}
}
