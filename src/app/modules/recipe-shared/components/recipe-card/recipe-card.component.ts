import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { RecipeShort } from '../../models/recipe-short.model';
import { Recipe } from '../../../recipes/models/recipe';

@Component({
  selector: 'app-recipe-card',
  templateUrl: './recipe-card.component.html',
  styleUrls: ['./recipe-card.component.less'],
})
export class RecipeCardComponent implements OnInit {
  @Input() public recipe!: Recipe;

  constructor() {}

  ngOnInit(): void {}
}
