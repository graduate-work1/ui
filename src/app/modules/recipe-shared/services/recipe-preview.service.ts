import { Inject, Injectable, Injector } from '@angular/core';
import { TuiDialogService } from '@taiga-ui/core';
import { RecipePreviewComponent } from '../components/recipe-preview/recipe-preview.component';
import { PolymorpheusComponent } from '@tinkoff/ng-polymorpheus';
import { RecipeShort } from '../models/recipe-short.model';

@Injectable({
  providedIn: 'root',
})
export class RecipePreviewService {
  constructor(
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
    @Inject(Injector) private readonly injector: Injector
  ) {}

  showDialog(recipe: RecipeShort): void {
    const dialog = this.dialogService.open<number>(
      new PolymorpheusComponent(RecipePreviewComponent, this.injector),
      {
        data: 237,
        dismissible: true,
        label: 'Heading',
      }
    );

    dialog.subscribe({
      next: (data) => {},
      complete: () => {},
    });
  }
}
