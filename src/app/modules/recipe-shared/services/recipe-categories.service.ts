import { Injectable } from '@angular/core';
import { RecipeCategoryEnum } from '../models/recipe-category.enum';
import { RecipeCategory } from '../models/recipe-category.model';

@Injectable({
  providedIn: 'root',
})
export class RecipeCategoriesService {
  constructor() {}

  public static getCategory(categoryEnum: RecipeCategoryEnum): RecipeCategory {
    switch (categoryEnum) {
      case RecipeCategoryEnum.Unknown:
        return {
          id: RecipeCategoryEnum.Unknown,
          title: 'Без категории',
          icon: 'assets/icons/pot-food.svg',
        };
      case RecipeCategoryEnum.Breakfasts:
        return {
          id: RecipeCategoryEnum.Breakfasts,
          title: 'Завтраки',
          icon: 'assets/icons/egg-fried.svg',
        };
      case RecipeCategoryEnum.Snacks:
        return {
          id: RecipeCategoryEnum.Snacks,
          title: 'Закуски',
          icon: 'assets/icons/cookie-bite.svg',
        };
      case RecipeCategoryEnum.Drinks:
        return {
          id: RecipeCategoryEnum.Drinks,
          title: 'Напитки',
          icon: 'assets/icons/wine-glass.svg',
        };
      case RecipeCategoryEnum.MainDishes:
        return {
          id: RecipeCategoryEnum.MainDishes,
          title: 'Основные блюда',
          icon: 'assets/icons/bowl-food.svg',
        };
      case RecipeCategoryEnum.Salads:
        return {
          id: RecipeCategoryEnum.Salads,
          title: 'Салаты',
          icon: 'assets/icons/salad.svg',
        };
      case RecipeCategoryEnum.Soups:
        return {
          id: RecipeCategoryEnum.Soups,
          title: 'Супы',
          icon: 'assets/icons/pot-food.svg',
        };
      case RecipeCategoryEnum.Desserts:
        return {
          id: RecipeCategoryEnum.Desserts,
          title: 'Выпечка и десерты',
          icon: 'assets/icons/cake-slice.svg',
        };
    }
  }
}
