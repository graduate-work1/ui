import { Inject, Injectable, Injector } from '@angular/core';
import { TuiDialogService } from '@taiga-ui/core';
import { Observable } from 'rxjs';
import { PolymorpheusComponent } from '@tinkoff/ng-polymorpheus';
import { ConfirmDialogComponent } from '../components/confirm-dialog/confirm-dialog.component';

@Injectable({
  providedIn: 'root',
})
export class ConfirmService {
  constructor(
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
    @Inject(Injector) private readonly injector: Injector
  ) {}

  public show(title: string, text: string): Observable<boolean> {
    return new Observable<boolean>((observer) => {
      const dialog = this.dialogService.open<boolean>(
        new PolymorpheusComponent(ConfirmDialogComponent, this.injector),
        {
          dismissible: true,
          label: title,
          data: text,
        }
      );

      dialog.subscribe({
        next: (data) => {
          observer.next(data);
        },
        complete: () => {
          observer.complete();
        },
      });
    });
  }
}
