import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, shareReplay, timer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ThemeSwitchService {
  public readonly isNightMode$: Observable<boolean>;
  private _isNightMode$: BehaviorSubject<boolean>;
  private readonly _key = 'isNightTheme';

  constructor() {
    this._isNightMode$ = new BehaviorSubject<boolean>(false);
    this.isNightMode$ = this._isNightMode$.asObservable().pipe(shareReplay(1));
    const isNight = localStorage.getItem(this._key) === 'true';
    this.setTheme(isNight);
  }

  public setTheme(isNight: boolean) {
    this._isNightMode$.next(isNight);
    localStorage.setItem(this._key, String(isNight));
  }
}
