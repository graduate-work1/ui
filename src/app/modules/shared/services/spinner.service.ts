import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SpinnerService {
  public showLoader$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  public text$: BehaviorSubject<string | null> = new BehaviorSubject<
    string | null
  >(null);

  constructor() {}

  public show(text: string | null = null): void {
    this.showLoader$.next(true);
    this.text$.next(text);
  }

  public hide(): void {
    this.showLoader$.next(false);
  }
}
