import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LayoutService {
  public isSidebarShown$ = new BehaviorSubject<boolean>(false);

  constructor() {}
}
