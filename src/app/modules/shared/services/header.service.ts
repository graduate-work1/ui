import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HeaderService {
  public showShadow$ = new BehaviorSubject<boolean>(true);
  public isWhite$ = new BehaviorSubject<boolean>(false);

  constructor() {}
}
