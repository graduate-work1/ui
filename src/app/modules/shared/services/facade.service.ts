import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FacadeService {
  public get userToken(): string | undefined {
    return this._userToken;
  }

  public set userToken(value: string | undefined) {
    if (typeof value === 'undefined') {
      return;
    }
    localStorage.setItem('token', value);
    this._userToken = value;
  }
  private _userToken: string | undefined;

  constructor() {
    const token = localStorage.getItem('token');
    if (token) {
      this._userToken = token;
    }
  }
}
