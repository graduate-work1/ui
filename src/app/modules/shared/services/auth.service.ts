import { Injectable } from '@angular/core';
import { first, Observable, tap } from 'rxjs';
import { SpinnerService } from './spinner.service';
import { UserService } from './user.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { FacadeService } from './facade.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private spinner: SpinnerService,
    private userService: UserService,
    private facadeService: FacadeService,
    private http: HttpClient
  ) {}

  public login(model: unknown): Observable<any> {
    this.spinner.show('Авторизация...');
    return this.http
      .post<any>(`${environment.apiUrl}/User/Authenticate`, model)
      .pipe(
        first(),
        tap((res: any) => {
          this.facadeService.userToken = res.token;
          this.userService.getSelf();
          this.spinner.hide();
        })
      );
  }

  public register(model: unknown): Observable<any> {
    this.spinner.show('Регистрация...');
    return this.http
      .post<any>(`${environment.apiUrl}/User/register`, model)
      .pipe(
        first(),
        tap((res: any) => {
          this.facadeService.userToken = res.token;
          this.userService.getSelf();
          this.spinner.hide();
        })
      );
  }

  public logout(): void {
    localStorage.removeItem('token');
  }
}
