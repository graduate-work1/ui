import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { SpinnerService } from './spinner.service';
import { NotificationService } from './notification.service';
import { Observable, tap } from 'rxjs';
import { FacadeService } from './facade.service';

@Injectable({
  providedIn: 'root',
})
export class InterceptorService implements HttpInterceptor {
  token: string | undefined;
  omitCalls = ['auth'];
  skipInterceptor = false;
  constructor(
    private router: Router,
    private facadeService: FacadeService,
    private notificationService: NotificationService,
    private _spinner: SpinnerService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.omitCalls.forEach((api) => {
      if (req.url.includes(api)) {
        this.skipInterceptor = true;
      }
    });
    this.token = this.facadeService.userToken;
    if (this.token || this.skipInterceptor) {
      const tokenizedReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + this.token),
      });
      return next.handle(tokenizedReq).pipe(
        tap(
          () => {},
          (err) => {
            if (err instanceof HttpErrorResponse) {
              if (err.status === 401) {
                this._spinner.hide();
                this.notificationService.showError('Вы не авторизованы');
                this.router.navigateByUrl('auth/login');
                return;
              }
              if (err.status === 500) {
                this._spinner.hide();
                this.notificationService.showError(
                  err.error?.message || err.error || err.message || 'Ошибка'
                );
                return;
              }
              this._spinner.hide();
              this.notificationService.showError(
                err.error?.message || err.error || err.message || 'Ошибка'
              );
            }
          }
        )
      );
    } else {
      // this.facadeService.userLoggedOut();
      // this.router.navigateByUrl('core/login');
    }
    return next.handle(req).pipe(
      tap(
        () => {},
        (err) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this._spinner.hide();
              this.notificationService.showError('Вы не авторизованы');
              this.router.navigateByUrl('auth/login');
              return;
            }
            if (err.status === 500) {
              this._spinner.hide();
              this.notificationService.showError(
                err.error?.message || err.error.split('at')[0] || err.message || 'Ошибка'
              );
              return;
            }
            this._spinner.hide();
            this.notificationService.showError(
              err.error?.message || err.error.split('at')[0] || err.message || 'Ошибка'
            );
          }
        }
      )
    );
  }
}
