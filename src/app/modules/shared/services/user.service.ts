import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, filter, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserModel } from '../models/user.model';
import { FacadeService } from './facade.service';
import { UserRoleEnum } from '../models/user-role.enum';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public currentUser$ = new BehaviorSubject<UserModel | null>(null);
  public isAuth$ = this.currentUser$.asObservable().pipe(filter((v) => !!v));
  public isAdmin$ = this.currentUser$.asObservable().pipe(
    filter((v) => !!v),
    map((u) => u?.roles.includes(UserRoleEnum.Admin))
  );
  public isDistributor$ = this.currentUser$.asObservable().pipe(
    filter((v) => !!v),
    map((u) => u?.roles.includes(UserRoleEnum.Distributor))
  );

  constructor(
    private httpClient: HttpClient,
    private facadeService: FacadeService
  ) {
    if (facadeService.userToken) {
      this.getSelf();
    }
  }

  public getSelf(): void {
    this.httpClient
      .get<UserModel>(`${environment.apiUrl}/user/self`)
      .subscribe((user) => this.currentUser$.next(user));
  }
}
