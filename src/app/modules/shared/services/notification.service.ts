import { Inject, Injectable } from '@angular/core';
import { TuiNotification, TuiNotificationsService } from '@taiga-ui/core';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor(
    @Inject(TuiNotificationsService)
    private readonly notificationsService: TuiNotificationsService
  ) {}

  public showError(msg: string): void {
    this.notificationsService
      .show(msg, {
        label: 'Ошибка',
        status: TuiNotification.Error,
        autoClose: 15000,
      })
      .subscribe(() => {});
  }

  public showSuccess(msg: string): void {
    this.notificationsService
      .show(msg, {
        status: TuiNotification.Success,
        autoClose: 5000,
      })
      .subscribe(() => {});
  }

  public show(
    notification: UserNotification,
    callback: (id: number) => void
  ): void {
    this.notificationsService
      .show(notification.text, {
        label: notification.title,
        status: notification.status,
        autoClose: 10000,
      })
      .subscribe({
        complete: () => {
          // callback(notification.id);
        },
      });
  }
}

export interface UserNotification {
  id: number;
  title: string;
  text: string;
  status: TuiNotification;
  isActive?: boolean;
  date?: string;
}
