import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';
import {
  TuiButtonModule,
  TuiDataListModule,
  TuiHostedDropdownModule,
  TuiLinkModule,
  TuiTextfieldControllerModule,
} from '@taiga-ui/core';
import { AddTimePipe } from './pipes/add-time.pipe';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {
  TuiAvatarModule,
  TuiDataListWrapperModule,
  TuiDropdownHoverModule,
  TuiInputModule,
  TuiMultiSelectModule,
  TuiSelectModule,
} from '@taiga-ui/kit';
import { TuiLetModule } from '@taiga-ui/cdk';
import { FooterComponent } from './components/footer/footer.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { SidebarAdminComponent } from './components/sidebar-admin/sidebar-admin.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { DiffToStrPipe } from './pipes/diff-to-str.pipe';

const components = [HeaderComponent, FooterComponent, SidebarAdminComponent];
const directives = [];
const pipes = [AddTimePipe];
const services = [];

@NgModule({
  declarations: [
    ...components,
    ...pipes,
    UserEditComponent,
    ConfirmDialogComponent,
    DiffToStrPipe,
  ],
  imports: [
    CommonModule,
    RouterModule,
    TuiLinkModule,
    TuiButtonModule,
    TuiAvatarModule,
    TuiLetModule,
    TuiDropdownHoverModule,
    TuiHostedDropdownModule,
    TuiDataListModule,
    AngularSvgIconModule,
    ReactiveFormsModule,
    TuiInputModule,
    TuiSelectModule,
    TuiDataListWrapperModule,
    TuiMultiSelectModule,
    TuiTextfieldControllerModule,
  ],
  providers: [HttpClientModule],
  exports: [...components, ...pipes, DiffToStrPipe],
  entryComponents: [ConfirmDialogComponent],
})
export class SharedModule {}
