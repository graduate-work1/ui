import { Pipe, PipeTransform } from '@angular/core';
import { DifficultyEnum } from '../../recipes/models/difficulty.enum';

@Pipe({
  name: 'diffToStr',
})
export class DiffToStrPipe implements PipeTransform {
  transform(value: DifficultyEnum, ...args: unknown[]): string {
    switch (value) {
      case DifficultyEnum.Unknown:
        return 'Неизвестно';
      case DifficultyEnum.Newbie:
        return 'Новичок';
      case DifficultyEnum.Easy:
        return 'Легко';
      case DifficultyEnum.Medium:
        return 'Средняя';
      case DifficultyEnum.Hard:
        return 'Сложно';
      case DifficultyEnum.Extra:
        return 'Очень сложно';
    }
  }
}
