import { Pipe, PipeTransform } from '@angular/core';
import { CookTimeModel } from '../models/cook-time.model';
import * as moment from 'moment';

@Pipe({
  name: 'addTime',
})
export class AddTimePipe implements PipeTransform {
  transform(value: CookTimeModel | any, ...args: unknown[]): unknown {
    return moment(0)
      .add(value.cookTime, value.cookPlural)
      .add(value.preparationTime, value.preparationPlural)
      .from(0)
      .replace('через ', '');
  }
}
