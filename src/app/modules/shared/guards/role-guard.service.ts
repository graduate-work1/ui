import { Inject, Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { filter, first, map, Observable } from 'rxjs';
import { UserRoleEnum } from '../models/user-role.enum';
import { UserService } from '../services/user.service';
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class RoleGuard implements CanActivate {
  constructor(private _userService: UserService, private _router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const role = route.data['role'];

    return this._userService.currentUser$.pipe(
      filter((v) => !!v),
      first(),
      map((u) => {
        const isInRole = (<UserModel>u).roles.includes(role);

        if (!isInRole) {
          this._router.navigateByUrl('/403');
        }

        return isInRole;
      })
    );
  }
}
