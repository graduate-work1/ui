import * as moment from 'moment';

export interface CookTimeModel {
  preparationTime: number;
  preparationPlural: moment.unitOfTime.DurationConstructor & string;
  cookTime: number;
  cookPlural: moment.unitOfTime.DurationConstructor & string;
}
