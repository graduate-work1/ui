export interface ProductShort {
  id: string;
  name: string;
  category: string;
  description: string;
  brand: string;
  producer: string;
  priceStart: number;
}
