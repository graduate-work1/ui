export class PagedRequest {
  public count!: number;
  public page!: number;
}
