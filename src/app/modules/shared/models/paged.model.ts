export interface Paged<T> {
  items: T[];
  totalCount: number;
}
