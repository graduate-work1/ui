import { ProductDto } from './product-dto';

export interface OrganizationProductDto {
  id: string;
  price: number;
  availability: number;
  organizationId: number;
  organizationName: string;
  product: ProductDto;
}
