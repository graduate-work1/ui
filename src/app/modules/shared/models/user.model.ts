import { UserRoleEnum } from './user-role.enum';
import { OrganizationShort } from '../../admin/models/organization-short.model';

export interface UserModel {
  id: string;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  password: string;
  roles: UserRoleEnum[];
  organization: OrganizationShort;
}
