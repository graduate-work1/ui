export interface ProductDto {
  id: string;
  barCode: number;
  name: string;
  category: string;
  description: string;
  brand: string;
  calories: number;
  proteins: number;
  fats: number;
  carbohydrates: number;
  weightNet: number;
  weightGross: number;
  ingredients: string;
  LWHmm: string;
  producer: string;
  countryOfProduction: string;
}
