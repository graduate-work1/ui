import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TuiDialogContext, TuiDialogService } from '@taiga-ui/core';
import { POLYMORPHEUS_CONTEXT } from '@tinkoff/ng-polymorpheus';
import { UserModel } from '../../models/user.model';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { UserRoleEnum } from '../../models/user-role.enum';
import { TUI_ARROW } from '@taiga-ui/kit';
import { AdminService } from '../../../admin/services/admin.service';
import { map, Observable, startWith } from 'rxjs';
import { OrganizationShort } from '../../../admin/models/organization-short.model';

@UntilDestroy()
@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.less'],
})
export class UserEditComponent implements OnInit {
  public form!: FormGroup;

  public roles = [UserRoleEnum.Distributor, UserRoleEnum.Admin];
  public organizations$!: Observable<OrganizationShort[]>;

  public isEdit = false;
  public isAdmin = false;

  readonly arrow = TUI_ARROW;
  public showOrganizations: boolean = false;

  constructor(
    private _fb: FormBuilder,
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
    @Inject(POLYMORPHEUS_CONTEXT)
    private readonly context: TuiDialogContext<
      UserModel,
      { user: UserModel; isEdit: boolean; isAdmin: boolean }
    >,
    private adminService: AdminService
  ) {}

  ngOnInit(): void {
    this.isEdit = this.context.data.isEdit;
    this.isAdmin = this.context.data.isAdmin;

    const formValue = this.context.data.user;

    this.form = this._fb.group({
      email: [
        this.isEdit ? formValue.email : '',
        Validators.compose([Validators.required, Validators.email]),
      ],
      firstName: [
        this.isEdit ? formValue.firstName : '',
        Validators.compose([Validators.required]),
      ],
      lastName: [
        this.isEdit ? formValue.lastName : '',
        Validators.compose([Validators.required]),
      ],
      userName: [
        this.isEdit ? formValue.userName : '',
        Validators.compose([Validators.required]),
      ],
      ...(this.isEdit
        ? {}
        : {
            password: [
              '',
              Validators.compose([
                Validators.required,
                Validators.minLength(8),
              ]),
            ],
          }),

      ...(this.isAdmin
        ? {
            roles: [this.isEdit ? formValue.roles : null],
          }
        : {
            roles: [[UserRoleEnum.Distributor]],
          }),
      ...(this.isAdmin
        ? {
            organization: [this.isEdit ? formValue.organization : null],
          }
        : {}),
    });

    this.form.controls['roles'].valueChanges
      .pipe(startWith(this.form.controls['roles'].value), untilDestroyed(this))
      .subscribe((value: UserRoleEnum[]) => {
        if (value?.includes(UserRoleEnum.Distributor)) {
          this.showOrganizations = true;
          this.form?.controls['organization'].setValidators(
            Validators.required
          );
          return;
        }

        this.showOrganizations = false;
        this.form?.controls['organization'].clearValidators();
      });

    if (this.isAdmin) {
      this.organizations$ = this.adminService
        .getOrganizationsPaged({ page: 1, count: 100 })
        .pipe(map((res) => res.items));
    }
  }

  submit(): void {
    const model = this.form?.getRawValue();
    model.organizationId = model.organization?.id || null;
    delete model.organization;
    this.context.completeWith(model);
  }
}
