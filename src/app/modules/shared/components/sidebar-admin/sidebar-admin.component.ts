import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { LayoutService } from '../../services/layout.service';

@Component({
  selector: 'app-sidebar-admin',
  templateUrl: './sidebar-admin.component.html',
  styleUrls: ['./sidebar-admin.component.less'],
})
export class SidebarAdminComponent implements OnInit, OnDestroy {
  @Input() public routes!: SidebarRouteData[];

  constructor(private layoutService: LayoutService) {}

  ngOnInit(): void {
    this.layoutService.isSidebarShown$.next(true);
  }

  ngOnDestroy() {
    this.layoutService.isSidebarShown$.next(false);
  }
}

export class SidebarRouteData {
  public title!: string;
  public link!: string;

  constructor(title: string, link: string) {
    this.title = title;
    this.link = link;
  }
}
