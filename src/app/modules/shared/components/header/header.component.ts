import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../../services/header.service';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
})
export class HeaderComponent implements OnInit {
  open = false;
  constructor(
    public headerService: HeaderService,
    public userService: UserService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {}

  logout() {
    this.authService.logout();
    window.location.href = '/';
  }
}
