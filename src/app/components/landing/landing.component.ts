import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../../modules/recipes/services/recipe.service';
import { Paged } from '../../modules/shared/models/paged.model';
import { Recipe } from '../../modules/recipes/models/recipe';
import { Observable } from 'rxjs';
import { ProductService } from '../../modules/products/services/product.service';
import { ProductShort } from '../../modules/shared/models/ProductShort';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.less'],
})
export class LandingComponent implements OnInit {
  public recipes$!: Observable<Paged<Recipe>>;
  public products$!: Observable<Paged<ProductShort>>;

  constructor(
    private _recipeService: RecipeService,
    private _productService: ProductService
  ) {}

  ngOnInit(): void {
    this.recipes$ = this._recipeService.getPaged({ page: 1, count: 4 });
    this.products$ = this._productService.getProductsPaged({
      page: 1,
      count: 4,
    });
  }
}
