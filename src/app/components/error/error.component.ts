import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Observable } from 'rxjs';
import { tuiPure } from '@taiga-ui/cdk';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorComponent implements OnInit {
  public data$: Observable<ErrorData> | undefined;
  constructor(private route: ActivatedRoute, private cdr: ChangeDetectorRef) {}

  public ngOnInit(): void {
    this.data$ = this.route.data.pipe(
      map(({ error }) => this.getErrorData(error))
    );
    this.cdr.markForCheck();
  }

  @tuiPure
  private getErrorData(errorCode: number): ErrorData {
    switch (errorCode) {
      case 404:
      default:
        return {
          code: 404,
          title: 'Страница не найдена',
          text: 'Кажется что-то пошло не так :(\nСтраница не найдена или еще не создана',
          image: 'book-reading',
        };
    }
  }
}

export interface ErrorData {
  code: number;
  title: string;
  text: string;
  image: string;
}
